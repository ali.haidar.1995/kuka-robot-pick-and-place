# Moving Objects With 6-DOF KUKA Robot Using ROS

## Objective

Commercially viable automated picking and stowing in unstructured environments, like picking products off shelves and putting them into shipping boxes, still remains a difficult challenge. The goal of the ARC is to perform simplified versions of the general task of picking and stowing items on shelves. As per *ARC Rules*: "The Challenge combines object recognition, pose recognition, grasp planning, compliant manipulation, motion planning, task planning, task execution, and error detection and recovery". 

The objective of this project is to demonstrate autonomous capability of the KR210 [serial manipulator](https://en.wikipedia.org/wiki/Serial_manipulator) in simulation to *pick and place* an object in a semi-unstructured environment.

Within the context of this project, a single *pick and place* cycle can be divided into the following tasks:

* Identify the target object on the shelf
* Plan and perform a clean movement towards the object
* Efficiently grasp/pick the target object without disturbing other objects
* Plan and perform a clean movement towards the drop-off site
* Efficiently stow/place the object at the drop-off site

## Environment Setup

The project uses [ROS Kinetic Kame](http://wiki.ros.org/kinetic) running on [Ubuntu 16.04 LTS (Xenial Xerus)](http://releases.ubuntu.com/16.04/).

The following tools are used for simulation and motion planning:

* [Gazebo](http://gazebosim.org/): a physics based 3D simulator extensively used in the robotics world, I used Gazebo 7.16.1.
* [RViz](http://wiki.ros.org/rviz): a 3D visualizer for sensor data analysis, and robot state visualization.
* [MoveIt!](http://moveit.ros.org/): a ROS based software framework for motion planning, kinematics and robot control.

## Launch process

1\. Create a catkin workspace:

```sh
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/
catkin_init_workspace
ls -l
```

2\. Clone or download project repository into the *src* directory of the catkin workspace

```sh
cd ~/catkin_ws/src
git clone https://github.com/ali.haidar.1995/pick-and-place.git
```

3\. Install missing dependencies if any

```sh
cd ~/catkin_ws
rosdep install --from-paths src --ignore-src --rosdistro=kinetic -y
```

4\. Change the permissions of script files to turn them executable

```sh
cd ~/catkin_ws/src/pick-place-robot/kuka_arm/scripts
sudo chmod u+x target_spawn.py
sudo chmod u+x IK_server.py
sudo chmod u+x safe_spawner.sh
```

5\. Build the project

```sh
cd ~/catkin_ws
catkin_make
```

6\. Open [.bashrc file](https://unix.stackexchange.com/questions/129143/what-is-the-purpose-of-bashrc-and-how-does-it-work) found in the *home* directory and add the following commands at the end

```sh
# Inform Gazebo (sim software) where to look for project custom 3D models
export GAZEBO_MODEL_PATH=~/catkin_ws/src/pick-place-robot/kuka_arm/models

# Auto-source setup.bash since the pick and place simulator spins up different nodes in separate terminals
source ~/catkin_ws/devel/setup.bash
```

7\. Save the .bashrc file and open a new terminal for changes to take effect

8\. Launch project by calling the safe_spawner shell script in a fresh terminal

```sh
$ cd ~/catkin_ws/src/pick-place-robot/kuka_arm/scripts
$ ./safe_spawner.sh
```

9\. Run the `IK_server` ROS node from a new terminal window 

```sh
cd ~/catkin_ws/src/pick-place-robot/kuka_arm/scripts
rosrun kuka_arm IK_server.py
```

10\. Arrange Gazebo and RViz windows side-by-side and click on **Next** button on left side of RViz to proceed between states. 

The status message in RViz changes as the different stages of simulation are traversed with the Next button. Actuation is observed in the Gazebo window.

------------